output "bucket_arn" {
  value       = aws_s3_bucket.example_melt.arn
  description = "Expected output after creation"
}
